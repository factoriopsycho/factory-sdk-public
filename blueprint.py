import argparse
import base64
import zlib
import json
import uuid



def parse_entities(bp: dict):
    # replace wiring/connections with generated uuid to allow us to easily create more of them potentially
    entities = bp['blueprint']['entities']
    entity_id_uuid_map = dict()
    parsed_entities = list()
    for entity in entities:
        idx = entity['entity_number']
        entity_id_uuid_map = str(uuid.uuid4())

    
    # update connections based on map
    for entity in entities:
        for connection in entity.get('connections').values():
            pass



def _decode_0(string):
    try:
        data = json.loads(
            zlib.decompress(
                base64.b64decode(string[1:])).decode('UTF-8'))
    except (TypeError, base64.binascii.Error, zlib.error):
        raise Exception(
            "Could not decode exchange string")
    return data


def _encode_0(obj):
    temp = bytes(json.dumps(obj), 'UTF-8')
    temp2 = zlib.compress(temp)
    temp3 = base64.b64encode(temp2)
    return '0' + temp3.decode('UTF-8')


def decode(string):
    try:
        return _decode[string[0]](string)
    except KeyError:
        raise InvalidExchangeString(
            "Could not decode exchange string")


def encode(obj):
    return _encode['latest'](obj)


_decode = {
    '0': _decode_0
}

_encode = {
    '0': _encode_0
}

_decode['latest'] = _decode['0']
_encode['latest'] = _encode['0']

"""
>>> parser = argparse.ArgumentParser(prog='PROG')
>>> parser.add_argument('--foo', action='store_true', help='foo help')
>>> subparsers = parser.add_subparsers(help='sub-command help')
>>>
>>> # create the parser for the "a" command
>>> parser_a = subparsers.add_parser('a', help='a help')
>>> parser_a.add_argument('bar', type=int, help='bar help')
>>>
>>> # create the parser for the "b" command
>>> parser_b = subparsers.add_parser('b', help='b help')
>>> parser_b.add_argument('--baz', choices='XYZ', help='baz help')"""

def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='blueprint functionality command', dest='command')

    decode_parser = subparsers.add_parser('decode', help='decode blueprint tools')
    decode_parser.add_argument('--output-json-file', type=str, default=None)
    decode_parser.add_argument('--pretty-print', action='store_true', default=False)
    decode_parser.add_argument('share_string', type=str)

    encode_parser = subparsers.add_parser('encode', help='encoding blueprint tools')
    encode_parser.add_argument('blueprint_json_file', help='path of blueprint json file')

    args = parser.parse_args()

    if args.command == 'decode':


        bp_dict = decode(args.share_string)
        output_func = print
        if args.output_json_file:
            def _inner(s: str):
                with open(args.output_json_file, 'w') as f:
                    f.write(s)
            output_func = _inner
        
        if args.pretty_print:
            output_str = json.dumps(bp_dict, indent=4)
        else:
            output_str = json.dumps(bp_dict)
        
        output_func(output_str)

    if args.command == 'encode':
        with open(args.blueprint_json_file, 'r') as f:
            blueprint_dict = json.load(f)
        print(encode(blueprint_dict))
    
if __name__ == '__main__':
    main()