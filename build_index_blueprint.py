import argparse
import os
import json

from blueprint import encode, decode


dataset_json_path = os.path.join(
    os.path.dirname(__file__), 'vanilla-0.15.12.json'
)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('index_blueprint_string', help='share string of index circuit blueprint')

    with open(dataset_json_path) as f:
        dataset_dict = json.load(f)

    entities = dataset_dict['entities']
    print(f'Found {len(entities)} entities')

if __name__ == '__main__':
    main()