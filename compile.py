import argparse
from dis import Instruction
import os
import json
import enum
from dataclasses import dataclass
from typing import Optional, List
from blueprint import encode, decode
from collections import namedtuple

class InstructionFormat(enum.Enum):
    R = 'R'
    I = 'I'
    J = 'J'

R = InstructionFormat.R
I = InstructionFormat.I
J = InstructionFormat.J

@dataclass
class InstructionDefinition:
    name: str
    opcode: int
    format: InstructionFormat
    funct: Optional[int] = 0


# TODO: Add the slt instructions and shift instructions
_instruction_set = (
    ('add', 0, R, 0x20),
    ('addi', 0x08, I),
    ('and', 0, R, 0x24),
    ('andi', 0x0C, I),
    ('beq', 0x04, I),
    ('blez', 0x06, I),
    ('bne', 0x05, I),
    ('bgtz', 0x07, I),
    ('div', 0, R, 0x1a),
    ('j', 0x02, J),
    ('jal', 0x03, J),
    ('jalr', 0x00, R, 0x09),
    ('jr', 0x00, R, 0x08),
    ('mult', 0x00, R, 0x18),
    ('nor', 0x00, R, 0x27),
    ('xor', 0x00, R, 0x26),
    ('or', 0x00, R, 0x25),
    ('ori', 0x0D, I),
    ('sub', 0x00, R, 0x22),
)


def getInstructionSet():
    return (InstructionDefinition(*i) for i in _instruction_set)

@dataclass
class ProgramInstruction:
    instruction: InstructionDefinition
    rs: Optional[int] = 0
    rt: Optional[int] = 0
    rd: Optional[int] = 0
    immediate: Optional[int] = 0
    shamt: Optional[int] = 0
    address: Optional[int] = 0

    @property
    def binary(self) -> str:
        def b(x, size=5):
            y = bin(x).lstrip('0b')
            if len(y) < size:
                y = ''.join(['0'] * (size-len(y))) + y
            return y
        inst = self.instruction
        fmt = inst.format
        if fmt == R:
            inst_binary = b(inst.opcode, 6) + b(self.rs) + b(self.rt) + b(self.rd) + b(self.shamt) + b(inst.funct, 6)
        elif fmt == I:
            inst_binary = b(inst.opcode, 6) + b(self.rs) + b(self.rt) + b(self.immediate, 16)
        elif fmt == J:
            inst_binary = b(inst.opcode, 6) + b(self.address, 26)
        return inst_binary

    @property
    def number(self) -> int:
        return int(self.binary, 2)


_registers = (
    'zero', 'at', 'v0', 'v1', 'a0', 'a1', 'a2', 'a3',
    't0', 't1', 't2', 't3', 't4', 't5', 't6', 't7',
    's0', 's1', 's2', 's3', 's4', 's5', 's6', 's7',
    't8', 't9', 'k0', 'k1', 'gp', 'sp', 'fp', 'ra'
)

def get_definition(instruction_name: str) -> InstructionDefinition:
    for i in getInstructionSet():
        if instruction_name.strip().lower() == i.name.lower():
            return i
    return None


class LineParseException(Exception):
    pass


class UnknownRegisterException(Exception):
    pass


def get_register_num(reg_name: str):
    r = reg_name.strip('$')
    if r not in _registers:
        raise UnknownRegisterException(f'No address found for register {reg_name}')
    return _registers.index(r)


def parse_line(line: str):
    # first get the instruction definition
    tokens = [t.strip(',').strip() for t in line.strip().split()]
    inst_name = tokens[0]
    inst_def = get_definition(inst_name)
    if not inst_def:
        raise LineParseException(f'Unable to get instruction definition for {inst_name}')

    result = {
        'instruction': inst_def
    }
    fmt = inst_def.format

    grn = get_register_num
    if fmt == R:
        result['rd'] = grn(tokens[1])
        result['rs'] = grn(tokens[2])
        result['rt'] = grn(tokens[3])
    elif fmt == I:
        result['rs'] = grn(tokens[1])
        result['rt'] = grn(tokens[2])
        result['immediate'] = int(tokens[3])
    elif fmt == J:
        result['address'] = int(tokens[2])

    return result


def compile(source_code: str) -> List[ProgramInstruction]:
    lines = source_code.split('\n')
    instructions = []
    for l in lines:
        res = parse_line(l)
        instruction = ProgramInstruction(**res)
        instructions.append(instruction)
    return instructions


class ProgramSizeException(BaseException):
    pass

def build_program_blueprint(rom_blueprint_json: dict, program_instructions: List[ProgramInstruction]) -> dict:
    # find all the constant combinators and set them all to 0
    # build ordered list of indices ordered in increasing y position
    cc_entity_indices = []
    cc_y_values = []
    entities = rom_blueprint_json['blueprint']['entities']

    def set_data_signal(entity_index: int, value: int):
        rom_blueprint_json['blueprint']['entities'][entity_index]['control_behavior']['filters'][0]['count'] = value
    
    for idx, e in enumerate(entities):
        if e['name'] == 'constant-combinator':
            cc_entity_indices.append(idx)
            cc_y_values.append(e['position']['y'])
            set_data_signal(idx, 0)

    sorted_entity_indices = [x[0] for x in sorted(zip(cc_entity_indices, cc_y_values), key=lambda x: x[0])]
    if len(program_instructions) > len(sorted_entity_indices):
        raise ProgramSizeException(f'Program ROM blueprint only holds {len(sorted_entity_indices)} instructions but program contains {len(program_instructions)}')
    
    for idx, pi in enumerate(program_instructions):
        n = pi.number
        entity_index = sorted_entity_indices[idx]
        set_data_signal(entity_index, n)
    return rom_blueprint_json


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--instruction_rom_blueprint_json', help='json for the instruction rom', default=os.path.join(os.path.dirname(__file__), 'blueprint_templates', 'instruction_rom.json'))
    parser.add_argument('source_code', help='MIPS assembly source code file')
    args = parser.parse_args()


    with open(args.source_code) as f:
        src_code_raw = f.read()

    program_instructions = compile(src_code_raw)
    with open(args.instruction_rom_blueprint_json) as f:
        rom_blueprint_json = json.load(f)
    compiled_blueprint_json = build_program_blueprint(rom_blueprint_json, program_instructions)
    print(encode(compiled_blueprint_json))

if __name__ == '__main__':
    main()